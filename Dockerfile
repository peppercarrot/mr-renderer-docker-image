FROM alpine:3.14
ENV RENDERLITE_URL https://framagit.org/peppercarrot/tools/raw/master/utils/renderfarm-lite.py?inline=false
RUN apk --no-cache add zip libressl python2 python3 py-pip git npm jq curl \
    ghostscript-fonts fontconfig inkscape imagemagick exiftool graphicsmagick \
    && apk add --no-cache --virtual .build-deps alpine-sdk \
    && npm install -g --unsafe-perm thumbsup \
    && apk del .build-deps alpine-sdk \
    && python3 -m pip install requests
RUN addgroup -g 1001 carrot \
    && adduser -G carrot -D -u 1001 carrot \
    && cd /home/carrot \
    && mkdir /home/carrot/.fonts && chown -R carrot: /home/carrot/.fonts \
    && wget -O renderfarm-lite.py ${RENDERLITE_URL} \
    && chmod +x /home/carrot/renderfarm-lite.py 
USER 1001

