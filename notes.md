# 2022-03-31 - updating node alpine

with alpine 3.15 it is not possible to build image in reason of `prebuild` binaries missing.

It seems to be related to *musl* compatibility which could be solved using debian version image.

alpine 3.14 is still maintained and is working so I keep this base image at the moment.
